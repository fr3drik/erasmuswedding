﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.IO;
using System.Text;

namespace ErasmusWedding.ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
			string emailBody = string.Empty;
			StringBuilder b = new StringBuilder();
			using (StreamReader reader = new StreamReader(@"C:\Projects\ErasmusWedding\ErasmusWedding.net\ErasmusWedding.Web\wwwroot\emailtemplates\erasmusweddinginvite.html"))
			{
				string line;
				while ((line = reader.ReadLine()) != null)
				{
					b.AppendLine(line);
				}
			}
			if (b.ToString().Contains("#datestamp#"))
				b.Replace("#datestamp", DateTime.Now.Ticks.ToString());
			if (b.ToString().Contains("##guestName##"))
				b.Replace("##guestName##", "Fredrik Erasmus");
			Send("fredrik8009@gmail.com", "Erasmus Wedding", b.ToString());
        }

		static void Send(string to, string subject, string body)
		{
			RestClient client = new RestClient();
			client.BaseUrl = new Uri("https://api.mailgun.net/v2");
			client.Authenticator = new HttpBasicAuthenticator("api", "key-0ab41377383596d7583d2ef543d939a6");
			RestRequest request = new RestRequest();
			request.AddParameter("domain", "mg.thefremus.net", ParameterType.UrlSegment);
			request.Resource = "{domain}/messages";
			request.AddParameter("from", "Fredrik Erasmus <fredrik@erasmuswedding.net>");
			request.AddParameter("to", to);
			request.AddParameter("subject", subject);
			request.AddParameter("html", body);
			request.Method = Method.POST;
			client.Execute(request);
		}
    }
}
