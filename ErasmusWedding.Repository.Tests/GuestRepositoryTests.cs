using DataAccess;
using ErasmusWedding.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace ErasmusWedding.Repository.Tests
{
    [TestClass]
    public class GuestRepositoryTests
    {
		IDbManager db;
		IGuestRepository<Guest> guestRepository;
		FakeLogger<GuestRepository<Guest>> guestRepositoryLogger;
		IConfiguration Configuration;

		[TestInitialize]
		public void Initialise()
		{
			var builder = new ConfigurationBuilder()
					.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

			Configuration = builder.Build();

			guestRepositoryLogger = new FakeLogger<GuestRepository<Guest>>();
			guestRepository = new GuestRepository<Guest>(guestRepositoryLogger, Configuration);
		}

		[TestMethod]
		public void SaveGuestTest_Insert()
		{
			var guest = new Guest {
				GuestKey = Guid.NewGuid().ToString(),
				Name = "xxxxxx",
				Email = "test@guest.co.za",
				ExtraGuests = 2,
				InviteDate = DateTime.Now,
				RsvpDate = DateTime.Now.AddDays(10),
				Rsvp = true,
				DateCreated = DateTime.Now,
				DateUpdated = DateTime.Now
			};
			var g = guestRepository.SaveGuest(guest);

			Assert.IsTrue(g.Status == System.Threading.Tasks.TaskStatus.RanToCompletion);
			Assert.IsNotNull(g.Result.First().GuestKey);
			Assert.AreEqual(guest.Name, g.Result.First().Name);
			Assert.AreEqual(guest.Email, g.Result.First().Email);
			Assert.AreEqual(guest.ExtraGuests, g.Result.First().ExtraGuests);
			Assert.IsNotNull(g.Result.First().InviteDate);
			Assert.IsNotNull(g.Result.First().RsvpDate);
			Assert.AreEqual(guest.Rsvp, g.Result.First().Rsvp);
		}

		[TestMethod]
		public void SaveGuestTest_Update()
		{
			var guest = new Guest
			{
				GuestKey = "e0e6dfe3-4ae9-4360-bef6-011eb91b2f2c",
				Name = "Adrian Tester",
				Email = "adrian@guest.co.za",
				ExtraGuests = 2,
				SongTitle = "Cry me a river",
				InviteDate = DateTime.Now,
				RsvpDate = DateTime.Now.AddDays(10),
				Rsvp = true,
				DateUpdated = DateTime.Now
			};
			var g = guestRepository.SaveGuest(guest);

			Assert.IsTrue(g.Status == System.Threading.Tasks.TaskStatus.RanToCompletion);
			Assert.IsNotNull(g.Result.First().GuestKey);
			Assert.AreEqual(guest.Name, g.Result.First().Name);
			Assert.AreEqual(guest.Email, g.Result.First().Email);
			Assert.AreEqual(guest.SongTitle, g.Result.First().SongTitle);
			Assert.AreEqual(guest.ExtraGuests, g.Result.First().ExtraGuests);
			Assert.IsNotNull(g.Result.First().InviteDate);
			Assert.IsNotNull(g.Result.First().RsvpDate);
			Assert.AreEqual(guest.Rsvp, g.Result.First().Rsvp);
		}
	}
}
