﻿using DataAccess;
using ErasmusWedding.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace ErasmusWedding.Repository.Tests
{
	[TestClass]
	public class SongRepositoryTests
	{
		IDbManager db;
		ISongRepository<Song> songRepository;
		FakeLogger<SongRepository<Song>> songRepositoryLogger;
		IConfiguration Configuration;

		[TestInitialize]
		public void Initialise()
		{
			var builder = new ConfigurationBuilder()
					.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

			Configuration = builder.Build();

			songRepositoryLogger = new FakeLogger<SongRepository<Song>>();
			songRepository = new SongRepository<Song>(songRepositoryLogger, Configuration);
		}

		[TestMethod]
		public void SaveSongTest_Insert()
		{
			var song = new Song
			{
				SongTitle = "The Best",
				DateCreated = DateTime.Now,
				DateUpdated = DateTime.Now
			};

			var g = songRepository.SaveSong(song);

			Assert.IsTrue(g.Status == System.Threading.Tasks.TaskStatus.RanToCompletion);
			Assert.IsNotNull(g.Result.First().SongKey);
			Assert.AreEqual(song.SongTitle, g.Result.First().SongTitle);
		}

		[TestMethod]
		public void SaveSongTest_Update()
		{
			var song = new Song
			{
				SongKey = "04f32785-41a0-4835-8372-a957acb5d69c",
				SongTitle = "Simply The Best",
				DateCreated = DateTime.Now,
				DateUpdated = DateTime.Now
			};
			var g = songRepository.SaveSong(song);

			Assert.IsTrue(g.Status == System.Threading.Tasks.TaskStatus.RanToCompletion);
			Assert.IsNotNull(g.Result.First().SongKey);
			Assert.AreEqual(song.SongTitle, g.Result.First().SongTitle);
		}
	}
}
