﻿using System;
using ErasmusWedding.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Dapper;
using System.Collections.Generic;

namespace ErasmusWedding.Repository
{
	public class SongRepository<T> : BaseRepository<T>, ISongRepository<T> where T : Song
	{
		public SongRepository(ILogger<SongRepository<T>> logger, IConfiguration configuration)
			:base(logger, configuration)
		{

		}

		public Task<IEnumerable<Song>> SaveSong(Song song)
		{
			try
			{
				using (dbManager.DbConnection)
				{
					var commandText = string.Empty;

					if(string.IsNullOrEmpty(song.SongKey))
					{
						song.SongKey = Guid.NewGuid().ToString();
						commandText = @"insert into song
						(
							song_key,
							song_title,
							date_created,
							date_updated
						)
						values
						(
							@songKey,
							@songTitle,
							@dateCreated,
							@dateUpdated
						);
						select
							song_key As SongKey,
							song_title As SongTitle,
							date_created As DateCreated,
							date_updated As DateUpdated
						from
							song";
					}
					else
					{
						commandText = 
					  @"update song
							set 
								song_title = @songTitle,
								date_updated = @dateUpdated
							where
								song_key = @songKey;
						select
							song_key As SongKey,
							song_title As SongTitle,
							date_created As DateCreated,
							date_updated As DateUpdated
						from
							song
						";
					}
					return dbManager.DbConnection.QueryAsync<Song>(commandText, 
					new {
						songKey = song.SongKey,
						songTitle = song.SongTitle,
						dateCreated = song.DateCreated,
						dateUpdated = song.DateUpdated
					});
				}
			}
			catch(Exception ex)
			{
				logger.LogError(ex, "An error occurred saving a song");
				throw ex;
			}
		}

		public Task<IEnumerable<Song>> GetSongs()
		{
			try
			{
				using (dbManager)
				{
					return dbManager.DbConnection.QueryAsync<Song>(
						@"select
							song_key As SongKey,
							song_title As SongTitle,
							date_created As DateCreated,
							date_updated As DateUpdated
						from
							song
						");
				}
			}
			catch(Exception ex)
			{
				logger.LogError(ex, "An error occurred trying to retrieve a list of songs");
				throw ex;
			}
		}
	}
}
