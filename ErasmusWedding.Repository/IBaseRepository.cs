﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.Configuration;

namespace ErasmusWedding.Repository
{
	public interface IBaseRepository<T>
	{
		IDbManager DbManager { get; }
		IList<ValidationResult> ValidationResults { get; }
		IConfiguration Configuration { get; }
	}
}
