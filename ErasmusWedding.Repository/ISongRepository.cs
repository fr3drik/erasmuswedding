﻿using System;
using System.Collections.Generic;
using System.Text;
using ErasmusWedding.Models;
using System.Threading.Tasks;

namespace ErasmusWedding.Repository
{
	public interface ISongRepository<T> : IBaseRepository<T> where T : Song
	{
		Task<IEnumerable<Song>> SaveSong(Song song);
		Task<IEnumerable<Song>> GetSongs();
	}
}
