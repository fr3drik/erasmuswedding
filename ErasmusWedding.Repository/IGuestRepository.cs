﻿using System;
using System.Collections.Generic;
using System.Text;
using ErasmusWedding.Models;
using System.Threading.Tasks;

namespace ErasmusWedding.Repository
{
    public interface IGuestRepository<T> : IBaseRepository<T>, IDisposable where T : Guest
    {
		Task<IEnumerable<Guest>> SaveGuest(Guest guest);
		Task<IEnumerable<Guest>> GetGuests();
    }
}
