﻿using System;
using ErasmusWedding.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Dapper;
using System.Collections.Generic;

namespace ErasmusWedding.Repository
{
	public class GuestRepository<T> : BaseRepository<T>, IGuestRepository<T> where T : Guest
	{
		public GuestRepository(ILogger<GuestRepository<T>> logger, IConfiguration configuration)
			: base(logger, configuration)
		{

		}

		public Task<IEnumerable<Guest>> SaveGuest(Guest guest)
		{
			try
			{
				var commandText = string.Empty;

				if (string.IsNullOrEmpty(guest.GuestKey))
				{
					guest.GuestKey = Guid.NewGuid().ToString();
					commandText = @"insert into guest
						(
							guest_key,
							guest_name,
							guest_email,
							guest_extra_guests,
							guest_song_name,
							guest_rsvp,
							guest_rsvp_date,
							guest_invite_date,
							date_created,
							date_updated
						)
						values
						(
							@guestKey,
							@guestName,
							@guestEmail,
							@guestExtraGuests,
							@guestSongName,
							@guestRsvp,
							@guestRsvpDate,
							@guestInviteDate,
							@dateCreated,
							@dateUpdated
						);
						select
							guest_key As GuestKey,
							guest_name As Name,
							guest_email As Email,
							guest_extra_guests As ExtraGuests,
							guest_song_name As SongName,
							guest_rsvp As Rsvp,
							guest_rsvp_date As RsvpDate,
							guest_invite_sent As InviteSent,
							guest_invite_date As InviteDate,
							date_created As DateCreated,
							date_updated As DateUpdated
						from
							guest";
				}
				else
				{
					commandText =
				  @"update guest
							set 
								guest_name = @guestName,
								guest_email = @guestEmail,
								guest_extra_guests = @guestExtraGuests,
								guest_song_name = @guestSongName,
								guest_rsvp = @guestRsvp,
								guest_rsvp_date = @guestRsvpDate,
								guest_invite_sent = @guestInviteSent,
								guest_invite_date = @guestInviteDate,
								date_updated = @dateUpdated
							where
								guest_key = @guestKey;
						select
							guest_key As GuestKey,
							guest_name As Name,
							guest_email As Email,
							guest_extra_guests As ExtraGuests,
							guest_song_name As SongName,
							guest_rsvp As Rsvp,
							guest_rsvp_date As RsvpDate,
							guest_invite_date As InviteDate,
							guest_invite_sent As InviteSent,
							date_created As DateCreated,
							date_updated As DateUpdated
						from
							guest
						";
				}
				return dbManager.DbConnection.QueryAsync<Guest>(commandText,
				new
				{
					guestKey = guest.GuestKey,
					guestName = guest.Name,
					guestEmail = guest.Email,
					guestExtraGuests = guest.ExtraGuests,
					guestSongName = guest.SongTitle,
					guestRsvp = guest.Rsvp,
					guestRsvpDate = guest.RsvpDate,
					guestInviteDate = guest.InviteDate,
					guestInviteSent = guest.InviteSent,
					dateCreated = guest.DateCreated,
					dateUpdated = guest.DateUpdated
				});
			}
			catch (Exception ex)
			{
				logger.LogError(ex, "An error occurred saving a guest");
				throw ex;
			}
		}

		public Task<IEnumerable<Guest>> GetGuests()
		{
			try
			{
				return dbManager.DbConnection.QueryAsync<Guest>(
					@"select
							guest_key As GuestKey,
							guest_name As Name,
							guest_email As Email,
							guest_extra_guests As ExtraGuests,
							guest_song_name As SongName,
							guest_rsvp As Rsvp,
							guest_rsvp_date As RsvpDate,
							guest_invite_sent As InviteSent,
							guest_invite_date As InviteDate,
							date_created As DateCreated,
							date_updated As DateUpdated
						from
							guest
						order by guest_name
						");
			}
			catch (Exception ex)
			{
				logger.LogError(ex, "An error occurred trying to retrieve a list of guests");
				throw ex;
			}
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				dbManager.Dispose();
			}
		}
	}
}
