﻿using DataAccess;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ErasmusWedding.Repository
{
	public abstract class BaseRepository<T> : IBaseRepository<T>
	{
		protected IDbManager dbManager;
		public IDbManager DbManager { get { return dbManager; } }

		protected ILogger<BaseRepository<T>> logger;
		public ILogger<BaseRepository<T>> Logger { get { return logger; } }

		internal IList<ValidationResult> validationResults;

		public IList<ValidationResult> ValidationResults
		{
			get { return validationResults; }
		}

		public IConfiguration Configuration { get { return configuration; } }
		private IConfiguration configuration;

		public BaseRepository(ILogger<BaseRepository<T>> logger, IConfiguration configuration)
		{
			this.logger = logger;
			this.configuration = configuration;
			dbManager = new DbManager(configuration.GetConnectionString("ErasmusWedding"));
		}
	}
}
