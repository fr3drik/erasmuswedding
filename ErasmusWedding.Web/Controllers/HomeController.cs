﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ErasmusWedding.Repository;
using ErasmusWedding.Models;
using Microsoft.Extensions.Logging;
using System.Text;
using System.IO;
using RestSharp;
using RestSharp.Authenticators;

namespace ErasmusWedding.net.Controllers
{
	public class HomeController : Controller
	{
		ILogger<GuestController> logger;
		IGuestRepository<Guest> guestRepository;
		ISongRepository<Song> songRepository;
		IGuestModel guestModel;

		public HomeController(IGuestRepository<Guest> guestRepository, ISongRepository<Song> songRepository, IGuestModel guestModel, ILogger<GuestController> logger)
		{
			this.logger = logger;
			this.guestRepository = guestRepository;
			this.songRepository = songRepository;
			this.guestModel = guestModel;
		}

		public async Task<IActionResult> Index(string guestKey = null)
		{
			var guests = await guestRepository.GetGuests();
			guestModel.Guests = guests;
			if (!string.IsNullOrEmpty(guestKey))
				guestModel.Guest = guests.Where(g => g.GuestKey == guestKey).FirstOrDefault();
			else
				guestModel.Guest = null;

			return View(guestModel);
		}

		public async Task<IActionResult> RsvpGuest(Guest guest)
		{
			if (guest == null || string.IsNullOrEmpty(guest.GuestKey))
			{
				logger.LogDebug("Failed rsvp: {@guest}", guest);
				return View("Index", guestModel);
			}

			try
			{
				logger.LogDebug("Guest rsvpéd: {@guest}", guest);
				guest.Rsvp = true;
				guest.RsvpDate = DateTime.Now;
				guest.DateUpdated = DateTime.Now;
				var guests = await guestRepository.SaveGuest(guest);

				if (!string.IsNullOrEmpty(guest.SongTitle))
					await songRepository.SaveSong(new Song { SongTitle = guest.SongTitle });

				guestModel = new GuestModel();
				guestModel.Guests = guests;
				if (!string.IsNullOrEmpty(guest.GuestKey))
					guest = guests.Where(g => g.GuestKey == guest.GuestKey).FirstOrDefault();

				guestModel.PostToken = Guid.NewGuid().ToString();

				return View("Index", guestModel);
			}
			catch (Exception ex)
			{
				logger.LogError("An error occurred saving a guest", ex);
				return new ObjectResult("An error occurred while saving a guest");
			}
		}

		public async Task<IActionResult> Guest(string guestKey = null)
		{
			IEnumerable<Guest> guests = new List<Guest>();
			var guest = new Guest();

			try
			{
				logger.LogDebug("Retrieving a list of guests");
				guests = await guestRepository.GetGuests();
				if (!string.IsNullOrEmpty(guestKey))
					guest = guests.Where(g => g.GuestKey == guestKey).FirstOrDefault();
			}
			catch (Exception ex)
			{
				logger.LogError(ex, "An error occurred while trying to retrieve the guest list");
			}

			guestModel.Guest = guest;
			guestModel.Guests = guests;
			return View(guestModel);
		}

		public async Task<IActionResult> SaveGuest(Guest guest)
		{
			try
			{
				logger.LogDebug("Guest saved: {@guest}", guest);
				guest.DateUpdated = DateTime.Now;
				var guests = await guestRepository.SaveGuest(guest);
				guestModel.Guests = guests;
				if (!string.IsNullOrEmpty(guest.GuestKey))
					guest = guests.Where(g => g.GuestKey == guest.GuestKey).FirstOrDefault();

				return View("Guest", guestModel);
			}
			catch (Exception ex)
			{
				logger.LogError("An error occurred saving a guest", ex);
				return new ObjectResult("An error occurred while saving a guest");
			}
		}

		public async Task<IActionResult> SaveSong(Song song)
		{
			try
			{
				logger.LogDebug("Song saved: {@song}", song);
				var songs = await songRepository.SaveSong(song);
				var songModel = new SongModel();
				songModel.Songs = songs;
				if (!string.IsNullOrEmpty(song.SongKey))
					songModel.Song = songs.Where(g => g.SongKey == song.SongKey).FirstOrDefault();

				return View("Song", songModel);
			}
			catch (Exception ex)
			{
				logger.LogError("An error occurred saving a guest", ex);
				return new ObjectResult("An error occurred while saving a guest");
			}
		}

		public async Task<IActionResult> Song(string songKey = null)
		{
			try
			{
				logger.LogDebug("Getting songs");
				var songs = await songRepository.GetSongs();
				var songModel = new SongModel();
				songModel.Songs = songs;
				if (!string.IsNullOrEmpty(songKey))
					songModel.Song = songs.Where(g => g.SongKey == songKey).FirstOrDefault();

				return View("Song", songModel);
			}
			catch (Exception ex)
			{
				logger.LogError("An error occurred getting songs", ex);
				return new ObjectResult("An error occurred while getting songs");
			}
		}

		public async Task<IActionResult> SendInvite(string guestKey)
		{
			try
			{
				using (guestRepository)
				{
					logger.LogDebug("Sending invite");
					var guests = await guestRepository.GetGuests();
					guestModel.Guests = guests;
					if (!string.IsNullOrEmpty(guestKey))
						guestModel.Guest = guests.Where(g => g.GuestKey == guestKey).FirstOrDefault();

					logger.LogDebug($"Invite template: {guestModel.InviteTemplatePath}");

					StringBuilder b = new StringBuilder();
					using (StreamReader reader = new StreamReader(guestModel.InviteTemplatePath))
					{
						string line;
						while ((line = reader.ReadLine()) != null)
						{
							b.AppendLine(line);
						}
					}

					if (b.ToString().Contains("#datestamp#"))
						b.Replace("#datestamp", DateTime.Now.Ticks.ToString());
					if (b.ToString().Contains("##guestName##"))
						b.Replace("##guestName##", guestModel.Guest.Name);
					if (b.ToString().Contains("##guestKey##"))
						b.Replace("##guestKey##", guestModel.Guest.GuestKey);

					Send(guestModel.Guest.Email, "Invitation - Erasmus Wedding", b.ToString());

					guestModel.Guest.InviteSent = true;
					guestModel.Guest.InviteDate = DateTime.Now;
					guestModel.Guest.DateUpdated = DateTime.Now;
					await guestRepository.SaveGuest(guestModel.Guest);
				}
				return View("Guest", guestModel);
			}
			catch (Exception ex)
			{
				logger.LogError("An error occurred while sending the invite", ex);
				return new ObjectResult("An error occurred while sending the invite");
			}
		}

		public async Task<IActionResult> Invite(string guestKey)
		{
			try
			{
				using (guestRepository)
				{
					logger.LogDebug("Getting invite");
					var guests = await guestRepository.GetGuests();
					guestModel.Guests = guests;
					if (!string.IsNullOrEmpty(guestKey))
						guestModel.Guest = guests.Where(g => g.GuestKey == guestKey).FirstOrDefault();					
				}
				return View("Invite", guestModel);
			}
			catch (Exception ex)
			{
				logger.LogError("An error occurred while sending the invite", ex);
				return new ObjectResult("An error occurred while sending the invite");
			}
		}

		private void Send(string to, string subject, string body)
		{
			RestClient client = new RestClient();
			client.BaseUrl = new Uri("https://api.mailgun.net/v2");
			client.Authenticator = new HttpBasicAuthenticator("api", "key-0ab41377383596d7583d2ef543d939a6");
			RestRequest request = new RestRequest();
			request.AddParameter("domain", "mg.thefremus.net", ParameterType.UrlSegment);
			request.Resource = "{domain}/messages";
			request.AddParameter("from", "Fredrik Erasmus <fredrik@erasmuswedding.net>");
			request.AddParameter("to", to);
			request.AddParameter("subject", subject);
			request.AddParameter("html", body);
			request.Method = Method.POST;
			client.Execute(request);
		}
	}

	public class GuestModel : IGuestModel
	{
		public Guest Guest { get; set; }
		public IEnumerable<Guest> Guests { get; set; }
		public string PostToken { get; set; }
		public string InviteTemplatePath { get; set; }
	}

	public interface IGuestModel
	{
		Guest Guest { get; set; }
		IEnumerable<Guest> Guests { get; set; }
		string PostToken { get; set; }
		string InviteTemplatePath { get; set; }
	}

	public class SongModel
	{
		public Song Song { get; set; }
		public IEnumerable<Song> Songs { get; set; }
	}
}