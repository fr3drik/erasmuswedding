﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ErasmusWedding.Repository;
using ErasmusWedding.Models;

namespace ErasmusWedding.net.Controllers
{
    public class AdminController : Controller
    {
		ILogger<GuestController> logger;
		IGuestRepository<Guest> guestRepository;
		ISongRepository<Song> songRepository;

		public AdminController(IGuestRepository<Guest> guestRepository, ISongRepository<Song> songRepository, ILogger<GuestController> logger)
		{
			this.logger = logger;
			this.guestRepository = guestRepository;
		}

	
	}

	public class GuestModel
	{
		public Guest Guest { get; set; }
		public IEnumerable<Guest> Guests { get; set; }
	}

	public class SongModel
	{
		public Song Song { get; set; }
		public IEnumerable<Song> Songs { get; set; }
	}
}