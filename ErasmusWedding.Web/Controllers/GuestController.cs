﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ErasmusWedding.Repository;
using ErasmusWedding.Models;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using System.IO;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace ErasmusWedding.net.Controllers
{
    [Produces("application/json")]
    [Route("api/guest")]
    public class GuestController : Controller
    {
		ILogger<GuestController> logger;
		IGuestRepository<Guest> guestRepository;

		public GuestController(IGuestRepository<Guest> guestRepository, ILogger<GuestController> logger)
		{
			this.logger = logger;
			this.guestRepository = guestRepository;
		}

		public async Task<IActionResult> Get()
		{
			try
			{
				logger.LogDebug("Retrieving a list of guests");
				var response = await guestRepository.GetGuests();
				return Ok(new { Guests = response });
			}
			catch (Exception ex)
			{
				logger.LogError(ex, "An error occurred while trying to retrieve the guest list");
				return new ObjectResult("An error occurred trying to retrieve the guest list");
			}
		}
	}
}