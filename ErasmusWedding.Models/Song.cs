﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ErasmusWedding.Models
{
    public class Song
    {
		public string SongKey { get; set; }
		public string SongTitle { get; set; }
		public DateTime DateCreated { get; set; }
		public DateTime DateUpdated { get; set; }
    }
}
