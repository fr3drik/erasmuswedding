﻿using System;

namespace ErasmusWedding.Models
{
    public class Guest
    {
		public string GuestKey { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public int ExtraGuests { get; set; }
		public string SongTitle { get; set; }
		public bool Rsvp { get; set; }
		public DateTime RsvpDate { get; set; }
		public bool InviteSent { get; set; }
		public DateTime InviteDate { get; set; }
		public DateTime DateCreated { get; set; }
		public DateTime DateUpdated { get; set; }
    }
}
