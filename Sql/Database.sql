CREATE DATABASE `erasmuswedding` /*!40100 COLLATE 'latin1_swedish_ci' */

CREATE USER 'erasmus'@'localhost' IDENTIFIED BY 'Password@123';
GRANT SELECT, INSERT, UPDATE, DELETE, DROP, CREATE TEMPORARY TABLES, EXECUTE  ON *.* TO 'erasmus'@'localhost';
FLUSH PRIVILEGES;
SHOW GRANTS FOR 'erasmus'@'localhost';

CREATE TABLE `guest` ( `guest_key` VARCHAR(255) NOT NULL,`guest_name` VARCHAR(255) NOT NULL,`guest_email` VARCHAR(255) NOT NULL,`guest_extra_guests` INT(11) NOT NULL,`guest_rsvp` BIT(1) NOT NULL,`guest_rsvp_date` DATETIME NULL DEFAULT NULL,`guest_invite_date` DATETIME NULL DEFAULT NULL,`date_created` DATETIME NULL DEFAULT NULL,`date_updated` DATETIME NULL DEFAULT NULL,PRIMARY KEY (`guest_key`)) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;


ALTER TABLE `guest` ADD COLUMN `guest_song_name` LONGTEXT NULL AFTER `guest_extra_guests`;

CREATE TABLE `song` (`song_key` VARCHAR(256) NOT NULL,`song_title` VARCHAR(255) NOT NULL,`date_created` DATETIME NULL DEFAULT NULL,`date_updated` DATETIME NULL DEFAULT NULL,PRIMARY KEY (`song_key`)) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

ALTER TABLE `guest`	ADD COLUMN `guest_invite_sent` BIT NULL DEFAULT NULL AFTER `guest_rsvp_date`;




